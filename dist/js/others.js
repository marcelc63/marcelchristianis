let profile = {
  subhead: "About Me",
  icon: "icon-vcard",
  color: "blue",
  content: [
    {
      type: "profile",
      name: "Marcel Christianis",
      birthdate: "March 6, 1996",
      location: "Jakarta, Indonesia",
      achievement: "Meeting Alexis Ohanian",
      future: "",
      contacts: "@marcelc63"
    }
  ]
};

let skills = {
  subhead: "Skills",
  icon: "icon-keyboard",
  color: "red",
  content: [
    {
      type: "skills",
      more: [
        {
          title: "Business",
          skills: [
            "Economics",
            "Accounting",
            "Finance",
            "Marketing",
            "Strategic Management",
            "Business Statistics"
          ]
        },
        {
          title: "Design",
          skills: ["Sketch", "Photoshop", "Illustrator", "Mockups & Wireframe"]
        },
        {
          title: "Technology",
          skills: [
            "AWS",
            "Node.js",
            "Vue.js",
            "React & React Native",
            "MongoDB & MySQL",
            "SASS",
            "PHP"
          ]
        },
        {
          title: "Communication",
          skills: [
            "Fluent English",
            "Native Bahasa Indonesian",
            "Comprehensive Writing"
          ]
        }
      ]
    }
  ]
};

let publication = {
  subhead: "Publication",
  icon: "icon-book-open",
  color: "yellow",
  content: [
    {
      title: "The Pastry Box Project",
      period: "20 December 2014",
      website: {
        name: "the-pastry-box-project.net",
        link:
          "https://the-pastry-box-project.net/marcel-christianis/2014-december-20"
      },
      subtitle: "What I Learn From My First Professional Work",
      description:
        "The Pastry Box Project is a renowned publication site that gathers 30 people who are each influential in their field and asks them to share thoughts regarding what they do, published everday througout the year at the rate of one per day. I was fortunate to had the chance to contribute and share my thoughts at The Pastry Box Project during their open submission period titled What I Learn From My First Professional Work."
    }
  ]
};

let education = {
  subhead: "Education",
  icon: "icon-college-graduation",
  color: "green",
  content: [
    {
      title: "University of Indonesia",
      period: "2019 - Present",
      subtitle: "Master of Management",
      description: "Currently pursuing Master of Management in Marketing.",
      more: [
        {
          title: "Scope of Study",
          content: [
            {
              subtitle: "Marketing",
              list: [
                "Strategic Management",
                "Product Development",
                "Consumer Behavior",
                "Marketing Research",
                "Communication and Promotion"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Royal Melbourne Institute of Technology",
      period: "2014 - 2018",
      subtitle: "Bachelor of Business with Distinction (B.Bus)",
      description:
        "Graduated Bachelor of Business with Distinction from Royal Melbourne Institute of Technology through Universitas Pelita Harapan and Royal Melbourne Institute of Technology's joint degree program.",
      more: [
        {
          title: "Scope of Study",
          content: [
            {
              subtitle: "Management",
              list: [
                "Strategic Management",
                "Supply Chain Management",
                "Intermediate Management",
                "Entrepreneurship",
                "Commercial Law"
              ]
            },
            {
              subtitle: "Marketing",
              list: ["Intermediate Marketing", "Consumer Behavior"]
            },
            {
              subtitle: "Economics",
              list: ["Intermediate Economics", "Behavioural Economics"]
            },
            {
              subtitle: "Accounting & Finance",
              list: [
                "Intermediate Accounting",
                "Intermediate Finance",
                "Financial Institutions",
                "Business Statistics"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Universitas Pelita Harapan",
      period: "2014 - 2018",
      subtitle: "Sarjana Ekonomi (S.E.)",
      description:
        "Graduated Sarjana Ekonomi from Universitas Pelita Harapan through Universitas Pelita Harapan and Royal Melbourne Institute of Technology's joint degree program.",
      more: [
        {
          title: "Scope of Study",
          content: [
            {
              subtitle: "Management",
              list: [
                "Strategic Management",
                "Supply Chain Management",
                "Intermediate Management",
                "Entrepreneurship",
                "Commercial Law"
              ]
            },
            {
              subtitle: "Marketing",
              list: ["Intermediate Marketing", "Consumer Behavior"]
            },
            {
              subtitle: "Economics",
              list: ["Intermediate Economics", "Behavioural Economics"]
            },
            {
              subtitle: "Accounting & Finance",
              list: [
                "Intermediate Accounting",
                "Intermediate Finance",
                "Financial Institutions",
                "Business Statistics"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Sekolah Pelita Harapan",
      type: "legacy",
      period: "Class of 2014",
      subtitle: "IB Bilingual Diploma",
      description:
        "Awarded IB Bilingual Diploma. Earned from completing six IB Subjects: English, History and Economics as High Level Subjects and Bahasa Indonesia, Physics and Mathematics as Standard Level Subjects."
    }
  ]
};
