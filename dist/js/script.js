// register modal component
Vue.component("modal", {
  template: "#modal-template",
  props: ["images", "caption"]
});

let app = new Vue({
  el: "#app",
  data: {
    title: "Hi",
    resume: [profile, education, experience, publication, skills],
    modal: {
      show: false,
      image: "",
      caption: ""
    }
  },
  methods: {
    show(image, caption) {
      this.modal.image = image;
      this.modal.caption = caption;
      this.modal.show = true;
    },
    hide() {}
  }
});
