let experience = {
  subhead: "Experience",
  icon: "icon-briefcase",
  color: "purple",
  content: [
    {
      title: "IconReel",
      period: "March 2017 - May 2019",
      website: {
        name: "iconreel.com",
        link: "https://iconreel.com"
      },
      images: [
        {
          thumb: "images/iconreel/home.jpg",
          image: "images/iconreel/iconreel.gif",
          caption: "IconReel Influencer Discovery Platform"
        },
        {
          thumb: "images/iconreel/filter.jpg",
          image: "images/iconreel/filter.jpg",
          caption: "IconReel Influencer Discovery Platform"
        },
        {
          thumb: "images/iconreel/filter2.jpg",
          image: "images/iconreel/filter2.jpg",
          caption: "IconReel Influencer Discovery Platform"
        }
      ],
      subtitle: "Software Engineer & Project Associate",
      description:
        "I work with a small team of engineers to build server infrastructure, database architecture, back-end platform, and front-end interface that facilitate large scale social media campaigns and host the profile of 20,000+ influencers.",
      more: [
        {
          title: "What is IconReel?",
          content: [
            {
              subtitle:
                "Indonesia's Largest Influencer Collective. IconReel connects brands with influencers for their social media campaign through our open technology platform. Record holder for the biggest Influencer Marketing Campaign in SEA for <a href=\"https://iconreel.com/blog/lazada-celebrates-1111-with-one-thousand-influencer-posts\" target=\"_blank\">Lazada 11.11 (SEA's version of Black Friday)</a> using 1000 Influencers.",
              list: []
            }
          ]
        },
        {
          title: "Responsibilities",
          content: [
            {
              subtitle: "As Software Engineer",
              list: [
                "Engineered Database that stored 20,000+ Influencers Data (MySQL)",
                "Build Scalable Server Infrastructure on AWS",
                "Back-end Platform for Quick Filtering (PHP)",
                "Real Time Data Analytics for every Influencer Profile",
                "Front-end & UI/UX Design (Photoshop, Sketch, SASS)"
              ]
            },
            {
              subtitle: "As Project Associate",
              list: [
                "Filed application to become Facebook Marketing Partner",
                "Client Pitching (Line, Uber, OPPO, among others.)",
                "Market Research, Profiling & CI",
                "Capacity Planning"
              ]
            }
          ]
        },
        {
          title: "What I've Learned",
          content: [
            {
              subtitle: "As Software Engineer",
              list: [
                "Creating a Data Store capable of housing 20,000+ data",
                "Using PHP at scale to facilitate large scale social media campaign",
                "Collaborating in team accross different technical roles with Git"
              ]
            },
            {
              subtitle: "As Project Associate",
              list: [
                "Filing an application to become Facebook Marketing Partner",
                "Client Pitching (Line, Uber, OPPO, among others.)",
                "Market Research, Profiling & CI",
                "Capacity Planning"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Analisa by IconReel",
      period: "March 2017 - Present",
      website: {
        name: "analisa.io",
        link: "https://analisa.io"
      },
      subtitle: "Software Engineer & Project Associate",
      description:
        "I created Analisa's data analytics engine that allows for real time social media data analysis. I team up with one other front-end developer to deliver a great social media analytics SaaS product.",
      images: [
        {
          thumb: "images/analisa/home.jpg",
          image: "images/analisa/analisa.gif",
          caption: "Analisa Social Media Analytics"
        },
        {
          thumb: "images/analisa/graph.jpg",
          image: "images/analisa/graph.jpg",
          caption: "Analisa Social Media Analytics"
        },
        {
          thumb: "images/analisa/graph2.jpg",
          image: "images/analisa/graph2.jpg",
          caption: "Analisa Social Media Analytics"
        }
      ],
      more: [
        {
          title: "What is Analisa?",
          content: [
            {
              subtitle:
                "Analisa is IconReel's Social Media Analytics SaaS. Provides deep Instagram analytics to any profiles and hashtags in Real Time. Offers sophisticated data visualization to any social media data.",
              list: []
            }
          ]
        },
        {
          title: "Responsibilities",
          content: [
            {
              subtitle: "As Software Engineer",
              list: [
                "Real Time Social Media Analytics (Node.js, Instagram API, Socket.IO)",
                "Scalable Data Store and Caching (MongoDB, S3, JSON)",
                "Back-end Platform for reccurring payment (Stripe)",
                "Data Visualization (Highcharts, D3, ES6 Babel)"
              ]
            },
            {
              subtitle: "As Project Associate",
              list: [
                "SaaS Business Model Research",
                "Go-To-Market Research",
                "Market Research, Profiling & CI",
                "Capacity Planning"
              ]
            }
          ]
        },
        {
          title: "What I've Learned",
          content: [
            {
              subtitle: "As Software Engineer",
              list: [
                "Create a live data stream with live data visualization",
                "Working with multiple social media API at an advance level",
                "Architect back-end platform for recurring payment"
              ]
            },
            {
              subtitle: "As Project Associate",
              list: [
                "Understanding the SaaS Model",
                "Bridging Technology Decision with Business Activities"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Mumu Indonesia",
      period: "Oct 2015 - March 2017",
      website: {
        name: "mumu.id",
        link: "https://mumud.id"
      },
      subtitle: "Marketing & UI/UX Designer",
      description:
        "I work as a part of the Marketing team delivering weekly discount and promotion through our web platform and email channel. I also help the Engineering team to work on front-end development for the main website.",
      images: [
        {
          thumb: "images/mumu/home.jpg",
          image: "images/mumu/home.jpg",
          caption: "Mumu Web Page"
        },
        {
          thumb: "images/mumu/shop.jpg",
          image: "images/mumu/shop.jpg",
          caption: "Mumu Web Page"
        },
        {
          thumb: "images/mumu/flyer.jpg",
          image: "images/mumu/flyer.jpg",
          caption: "Mumu Promotional Flyer"
        },
        {
          thumb: "images/mumu/email.jpg",
          image: "images/mumu/email.jpg",
          caption: "Mumu Promotional Email"
        }
      ],
      more: [
        {
          title: "What is Mumu?",
          content: [
            {
              subtitle:
                "<i><u>Note: Mumu Indonesia is now defunct and pivoted to become IconReel.</u></i> Grocery delivery under 1 hour. Mumu Indonesia ran in 5 Tier-1 and Tier-2 Cities in Indonesia: Jakarta, Medan, Bandung, Surabaya, and Bogor. Offers more than +1000 SKUs from Carrefoure, Ramayana, and Lotte among others.",
              list: []
            }
          ]
        },
        {
          title: "Responsibilities",
          content: [
            {
              subtitle: "UI/UX Designer",
              list: [
                "Create landing page for seasonal events",
                "Update website with latest promotion and discount",
                "Build HTML for Email Marketing campaign (via SendGrid)",
                "Blast Email to database of 10,000+ contacts",
                "Create Graphic for Social Media & Flyers",
                "Illustrate Icons"
              ]
            },
            {
              subtitle: "As Marketing",
              list: [
                "Run Email Marketing Campaign (including Copywriting)",
                "Funnel Optimization",
                "Market Research, Profiling & CI",
                "Pitch, Retailer Onboarding & Customer Service"
              ]
            }
          ]
        },
        {
          title: "What I've Learned",
          content: [
            {
              subtitle: "As UI/UX Designer",
              list: [
                "Aligning web design with marketing agendas",
                "Well thought out design to communicate with 10,000+ contacts"
              ]
            },
            {
              subtitle: "As Project Associate",
              list: ["Run Email Marketing Campaign", "Talking to Customer"]
            }
          ]
        }
      ]
    },
    {
      title: "Sempoa Sip",
      type: "legacy",
      period: "Summer 2014",
      subtitle: "Lead Designer & Front-end Developer",
      description:
        "Experienced a two months paid internship from 9 a.m. to 5 p.m. in their office as a lead designer and front-end developer. Responsible for the User Interface and Design of the company's new website and administration system. Spent a month working with a group of developers, marketing team and directors."
    },
    {
      title: "LSKTIK",
      type: "legacy",
      period: "Summer 2013",
      subtitle: "Graphic Design",
      website: {
        name: "lsktik.com",
        link: "https://lsktik.com"
      },
      description:
        "Spent one month internship working with a team of designers with the goal of creating the company's branding guide which consist of standardized logo, fonts, color, graphics and other graphic element to be used on all of the company's media."
    },
    {
      title: "PT. Cyberindo Aditama",
      type: "legacy",
      period: "Summer 2010",
      subtitle: "Trainee Network Engineer",
      website: {
        name: "cbn.id",
        link: "https://cbn.id"
      },
      description:
        "Spent two weeks internship learning the basic of networking directly from network engineers at an internet service provider. Had the chance to look at the server that made our online experience possible and also play around with routers and servers."
    }
  ]
};
