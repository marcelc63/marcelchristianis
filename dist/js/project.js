let project = {
  subhead: "Side Project",
  icon: "icon-rocket-1",
  color: "orange",
  content: [
    {
      title: "Coinstory",
      website: {
        name: "coinstoryapp.com",
        link: "https://coinstoryapp.com"
      },
      subtitle: "Crypto News with Instagram's Story and Carousel Format",
      description:
        "Coinstory is a cryptocurrency app for those who are not yet in cryptocurrencies. I realized that a lot of people in the households are starting to invest in cryptocurrencies. However, they are not well informed with the knowledge of cryptocurrencies and have little to no capacity to read comprehensive articles. I created a platform where everybody can read the latest information about their portfolio in a format that they are already familiar with.",
      images: [
        {
          thumb: "images/coinstory/ads.png",
          image: "images/coinstory/ads.png",
          caption: "Instagram Ads"
        },
        {
          thumb: "images/coinstory/appstore.png",
          image: "images/coinstory/appstore.png",
          caption: "App Store Promotion Image"
        },
        {
          thumb: "images/coinstory/app1.png",
          image: "images/coinstory/app1.png",
          caption: "Coinstory Mobile App"
        },
        {
          thumb: "images/coinstory/app2.png",
          image: "images/coinstory/app2.png",
          caption: "Instagram Story-like UI"
        },
        {
          thumb: "images/coinstory/app3.png",
          image: "images/coinstory/app3.png",
          caption: "Data from Coin Market Cap API"
        }
      ],
      more: [
        {
          title: "What I Did",
          content: [
            {
              subtitle: "Web Scraping",
              list: [
                "Web Scraping multiple web sources (Node Module X-Ray)",
                "API to deliver content to mobile application",
                "Auto Translate scraped content to Bahasa Indonesia with Google Translate API",
                "Auto Summarize scraped content"
              ]
            },
            {
              subtitle: "Mobile Application (iOS and Android)",
              list: [
                "React Native with Expo",
                "Extensive use of React Animation",
                "Utilize Coin Market Cap API"
              ]
            }
          ]
        },
        {
          title: "What I've Learned",
          content: [
            {
              subtitle: "Web Scraping",
              list: [
                "Create modular API to efficiently scrape",
                "Automate web scraping on server"
              ]
            },
            {
              subtitle: "React Native",
              list: [
                "Complete the creation of a mobile application for iOS and Android using React Native",
                "Working with React Native Animation API to create fluid and interactive gestures",
                "Create Story-like gesture on mobile with React Native"
              ]
            },
            {
              subtitle: "Marketing",
              list: [
                "Run and Optimize Instagram Ads",
                "Create and execute a Road-to-Market strategy"
              ]
            }
          ]
        },
        {
          title: "Future Roadmap",
          content: [
            {
              subtitle: "App & Tools",
              list: [
                "Portfolio tracker",
                "Profit & loss calculator",
                "News Source Customization"
              ]
            },
            {
              subtitle: "Road-to-Market Strategy",
              list: [
                "Engage Telegram communities",
                "Promote with Crypto Influencers"
              ]
            }
          ]
        }
      ]
    },
    {
      title: "Logangster",
      website: {
        name: "logangster.com",
        link: "https://logangster.com"
      },
      subtitle:
        "Watch YouTube Video in Sync while Chatting and Unlock Fun Emojis",
      description:
        '<i><u>Note: This project started months before Logan Paul\'s controversial video got published.</u></i> Inspired by <a href="https://247caseyneistat.com" target="_blank">247caseyneistat.com</a>, I wanted to create a project that tap the strong fanbase that Logan Paul has. I created a platform where anybody can watch his videos in sync while chat.',
      images: [
        {
          thumb: "images/logangster/emoji.jpg",
          image: "images/logangster/emoji.jpg",
          caption: "Gamification with Unlockable Emojis"
        },
        {
          thumb: "images/logangster/scoreboard.jpg",
          image: "images/logangster/scoreboard.jpg",
          caption: "Gamification with Scoreboard"
        },
        {
          thumb: "images/logangster/giveaway.jpg",
          image: "images/logangster/giveaway.jpg",
          caption: "Giveaway Event on Instagram"
        },
        {
          thumb: "images/logangster/app.jpg",
          image: "images/logangster/app.jpg",
          caption: "Logangster Mobile App"
        },
        {
          thumb: "images/logangster/app2.jpg",
          image: "images/logangster/app2.jpg",
          caption: "Logangster Mobile App"
        },
        {
          thumb: "images/logangster/influencer.jpg",
          image: "images/logangster/influencer.jpg",
          caption: "Promotion with Fan Accounts (~20k Followers)"
        }
      ],
      more: [
        {
          title: "What I Did",
          content: [
            {
              subtitle: "Community Management",
              list: [
                "Held and managed Merchandise Giveaway",
                "Moderate conversation on chat"
              ]
            },
            {
              subtitle: "Real Time Chat",
              list: ["Using Socket.IO", "Manage authentication with PassportJS"]
            },
            {
              subtitle: "In Sync Video Streaming",
              list: [
                "Retrieve Video information with YouTube API",
                "Custom algorithm to manage with JavaScript",
                "Inspired from 247caseyneistat.com"
              ]
            },
            {
              subtitle: "Mobile App with Swift (iOS)",
              list: [
                "Socket.IO in Swift",
                "Recreate Syncing algorithm with Swift"
              ]
            }
          ]
        },
        {
          title: "What I've Learned",
          content: [
            {
              subtitle: "Being too dependent on a Creator",
              list: [
                "This project downfall was aligned with Logan Paul's downfall"
              ]
            },
            {
              subtitle: "Gamification",
              list: [
                "Incentivized community with unlockable emojis",
                "Explore the perfect interval before unlocking"
              ]
            },
            {
              subtitle: "Influencer Marketing",
              list: [
                "Approached Fan Accounts on Instagram with 15,000+ followers to help with promotion"
              ]
            },
            {
              subtitle: "Community Management",
              list: [
                "Handling a community and keeping conversation steady is hard",
                "Moderating giveaway fairly while maintaining incentives is not easy"
              ]
            }
          ]
        },
        {
          title: "Future Roadmap",
          content: [
            {
              subtitle: "Facilitate More Creators",
              list: [
                "Shift away from a Logan Paul focus app and offer feature to all fanbase"
              ]
            },
            {
              subtitle: "Extend Gamification",
              list: ["Gamification works, double down on this effort"]
            }
          ]
        }
      ]
    }
  ]
};
